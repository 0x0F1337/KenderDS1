﻿using Scribe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace KenderDS1.GUI
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : Application
    {
        internal static Logger logger;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            logger = new Logger();
        }
    }
}
