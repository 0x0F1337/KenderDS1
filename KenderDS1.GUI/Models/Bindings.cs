﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace KenderDS1.GUI.Models
{
    public class Bindings
    {
        private const string DLL_NAME = "KenderDS1.Unmanaged.dll";

        private enum MemoryOffsets
        {
            VIT = 0x38,
            ATT = 0x40,
            END = 0x48,
            STR = 0x50,
            DEX = 0x58,
            INT = 0x60,
            FAI = 0x68,
            RES = 0x80,

            LEVEL = 0x88,
            SOULS = 0x8C,
            HUMANITIES = 0x120
        }


        [DllImport(DLL_NAME, CallingConvention = CallingConvention.Cdecl)]
        /// <summary>
        /// Gets a stat, given its memory offset
        /// </summary>
        /// <param name="offset">The offset of the stat</param>
        /// <returns>The stat value</returns>
        private static extern int GetStat(int offset);

        [DllImport(DLL_NAME, CallingConvention = CallingConvention.Cdecl)]
        /// <summary>
        /// Sets a stat, given its memory offset and value
        /// </summary>
        /// <param name="offset">The offset of the stat</param>
        /// <param name="value">Value to be set</param>
        private static extern int SetStat(int offset, int value);


        public static int GetVitality()     => GetStat((int)MemoryOffsets.VIT);
        public static int GetAttunement()   => GetStat((int)MemoryOffsets.ATT);
        public static int GetEndurance()    => GetStat((int)MemoryOffsets.END);
        public static int GetStrength()     => GetStat((int)MemoryOffsets.STR);
        public static int GetDexterity()    => GetStat((int)MemoryOffsets.DEX);
        public static int GetIntelligence() => GetStat((int)MemoryOffsets.INT);
        public static int GetFaith()        => GetStat((int)MemoryOffsets.FAI);
        public static int GetResistance()   => GetStat((int)MemoryOffsets.RES);
        public static int GetLevel()        => GetStat((int)MemoryOffsets.LEVEL);
        public static int GetSouls()        => GetStat((int)MemoryOffsets.SOULS);
        public static int GetHumanities()   => GetStat((int)MemoryOffsets.HUMANITIES);


        public static int SetVitality(int value)     => SetStat((int)MemoryOffsets.VIT, value);
        public static int SetAttunement(int value)   => SetStat((int)MemoryOffsets.ATT, value);
        public static int SetEndurance(int value)    => SetStat((int)MemoryOffsets.END, value);
        public static int SetStrength(int value)     => SetStat((int)MemoryOffsets.STR, value);
        public static int SetDexterity(int value)    => SetStat((int)MemoryOffsets.DEX, value);
        public static int SetIntelligence(int value) => SetStat((int)MemoryOffsets.INT, value);
        public static int SetFaith(int value)        => SetStat((int)MemoryOffsets.FAI, value);
        public static int SetResistance(int value)   => SetStat((int)MemoryOffsets.RES, value);
        public static int SetLevel(int value)        => SetStat((int)MemoryOffsets.LEVEL, value);
        public static int SetSouls(int value)        => SetStat((int)MemoryOffsets.SOULS, value);
        public static int SetHumanities(int value)   => SetStat((int)MemoryOffsets.HUMANITIES, value);
    }
}
