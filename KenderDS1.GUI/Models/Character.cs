﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KenderDS1.GUI.Models
{
    public class Character : NotifableModel
    {
        private int level;
        public int Level
        {
            get => level;
            set
            {
                level = value;
                NotifyPropertyChanged();
            }
        }

        private int souls;
        public int Souls
        {
            get => souls;
            set
            {
                souls = value; ;
                NotifyPropertyChanged();
            }
        }

        private int humanities;
        public int Humanities
        {
            get => humanities;
            set
            {
                humanities = value;
                NotifyPropertyChanged();
            }
        }

        private Stats stats = new Stats();

        public Stats Stats
        {
            get => stats;
            set
            {
                stats = value;
                NotifyPropertyChanged();
            }
        }
    }
}
