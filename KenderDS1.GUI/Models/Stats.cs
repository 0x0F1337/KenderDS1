﻿namespace KenderDS1.GUI.Models
{
    public class Stats : NotifableModel
    {
        private int vitality;
        public int Vitality
        {
            get => vitality;
            set
            {
                vitality = value;
                NotifyPropertyChanged();
            }
        }

        private int attunement;
        public int Attunement
        {
            get => attunement;
            set
            {
                attunement = value;
                NotifyPropertyChanged();
            }
        }

        private int endurance;
        public int Endurance
        {
            get => endurance;
            set
            {
                endurance = value;
                NotifyPropertyChanged();
            }
        }

        private int strength;
        public int Strength
        {
            get => strength;
            set
            {
                strength = value;
                NotifyPropertyChanged();
            }
        }

        private int dexterity;
        public int Dexterity
        {
            get => dexterity;
            set
            {
                dexterity = value;
                NotifyPropertyChanged();
            }
        }

        private int resistance;
        public int Resistance
        {
            get => resistance;
            set
            {
                resistance = value;
                NotifyPropertyChanged();
            }
        }

        private int intelligence;
        public int Intelligence
        {
            get => intelligence;
            set
            {
                intelligence = value;
                NotifyPropertyChanged();
            }
        }

        private int faith;
        public int Faith
        {
            get => faith;
            set
            {
                faith = value;
                NotifyPropertyChanged();
            }
        }
    }
}
