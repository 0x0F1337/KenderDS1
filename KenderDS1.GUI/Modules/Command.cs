﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace KenderDS1.GUI.ViewModels
{
    public class Command : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly Action callback;


        public Command(Action callback)
        {
            this.callback = callback;
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }


        public void Execute(object parameter)
        {
            callback();
        }
    }
}
