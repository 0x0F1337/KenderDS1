﻿using KenderDS1.GUI.Models;
using System;

namespace KenderDS1.GUI.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly string[] QUOTES = { "Praise the sun!", "YOU DIED", "Don't you dare go Hollow.", "Mmm...mmm...", "You, you! Give me, warm!", "Yulia! Yulia!" };


        public string WindowTitle { get; private set; }

        private Character character;
        public Character Character
        {
            get => character;
            set
            {
                character = value;
                NotifyPropertyChanged();
            }
        }

        public Command ApplyChangedCmd => new Command(() => ApplyChanges(Character));
        public Command ResetCmd => new Command(() => Character = InitializeCharacterInfo());


        public MainViewModel()
        {
            Character = InitializeCharacterInfo();
            WindowTitle = PickRandomQuote(QUOTES);
        }


        /// <summary>
        /// Retrieves the character information from the Dark Souls process
        /// </summary>
        /// <returns>Information of the character</returns>
        private Character InitializeCharacterInfo()
        {
            Character character = new Character();
            Stats stats = character.Stats;

            character.Level      = Bindings.GetLevel();
            character.Souls      = Bindings.GetSouls();
            character.Humanities = Bindings.GetHumanities();

            stats.Vitality     = Bindings.GetVitality();
            stats.Attunement   = Bindings.GetAttunement();
            stats.Endurance    = Bindings.GetEndurance();
            stats.Strength     = Bindings.GetStrength();
            stats.Dexterity    = Bindings.GetDexterity();
            stats.Resistance   = Bindings.GetResistance();
            stats.Intelligence = Bindings.GetIntelligence();
            stats.Faith        = Bindings.GetFaith();

            return character;
        }


        /// <summary>
        /// Picks a random quote from a collection
        /// </summary>
        /// <param name="quotes">Collection of quotes</param>
        /// <returns>Random quote from the collection</returns>
        private string PickRandomQuote(string[] quotes)
        {
            Random rand = new Random();
            int r = rand.Next(0, quotes.Length);

            return quotes[r];
        }


        /// <summary>
        /// Apply all changes made to the stats of a character
        /// </summary>
        /// <param name="character">Information to be applied</param>
        private void ApplyChanges(Character character)
        {
            Stats stats = character.Stats;

            Bindings.SetLevel(character.Level);
            Bindings.SetSouls(character.Souls);
            Bindings.SetHumanities(character.Humanities);

            Bindings.SetVitality(stats.Vitality);
            Bindings.SetAttunement(stats.Attunement);
            Bindings.SetEndurance(stats.Endurance);
            Bindings.SetStrength(stats.Strength);
            Bindings.SetDexterity(stats.Dexterity);
            Bindings.SetResistance(stats.Resistance);
            Bindings.SetIntelligence(stats.Intelligence);
            Bindings.SetFaith(stats.Faith);
        }
    }
}
