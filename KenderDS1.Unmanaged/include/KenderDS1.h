#pragma once


#include <WinProc.h>
#include "Offsets.h"


class KenderDS1 {
private:
	const LPSTR DS_PROCESS_NAME = (LPSTR)"DARKSOULS.exe";

	HANDLE hDarkSouls = NULL;
	LPVOID baseAddress = NULL;
	MODULEINFO mInfo;

	/// <summary>
	/// Initializes the class instance, getting a HANDLE for the running Dark Souls process, its main module info and the static address
	/// from which the information will be retrived
	/// </summary>
	void Initialize();


public:
	KenderDS1();


	/// <summary>
	/// Gets a stat given its memory offset
	/// </summary>
	/// <param name="offset">The offset of the stat</param>
	/// <returns>The stat value</returns>
	int GetStat(int offset);

	/// <summary>
	/// Sets a stat, given its memory offset and value
	/// </summary>
	/// <param name="offset">The offset of the stat</param>
	/// <param name="value">Value to be set</param>
	void SetStat(int offset, int value);
};


extern "C" {
	DllExport int GetStat(int offset) {
		KenderDS1 kender;
		return kender.GetStat(offset);
	}


	DllExport void SetStat(int offset, int value) {
		KenderDS1 kender;
		return kender.SetStat(offset, value);
	}
}