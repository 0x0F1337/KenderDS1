#pragma once


#define CHARACTER_OFFSET 0x0F78700

#define STATS_OFFSET 0x08


#define VIT_OFFSET 0x38
#define ATT_OFFSET 0x40

#define END_OFFSET 0x48
#define STR_OFFSET 0x50

#define DEX_OFFSET 0x58
#define INT_OFFSET 0x60

#define FAI_OFFSET 0x68

#define RES_OFFSET 0x80

#define LEVEL_OFFSET 0x88

#define SOULS_OFFSET 0x8C

#define HUMANITIES_OFFSET 0x120
