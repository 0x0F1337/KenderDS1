#include "KenderDS1.h"


KenderDS1::KenderDS1() {
	Initialize();
}


void KenderDS1::Initialize() {
	hDarkSouls = GetProcessHandle(KenderDS1::DS_PROCESS_NAME);
	mInfo = GetMainModuleInfo(KenderDS1::DS_PROCESS_NAME, KenderDS1::DS_PROCESS_NAME);

	LPVOID aux;
	DWORD staticAddr = (DWORD_PTR)mInfo.lpBaseOfDll + CHARACTER_OFFSET;
	ReadProcessMemory(hDarkSouls, (LPCVOID)staticAddr, &aux, sizeof(aux), NULL);

	DWORD aux2 = (DWORD)aux + STATS_OFFSET;
	ReadProcessMemory(hDarkSouls, (LPCVOID)aux2, &baseAddress, sizeof(baseAddress), NULL);
}


int KenderDS1::GetStat(int offset) {
	LPVOID stat = NULL;
	DWORD statAddr = (DWORD)baseAddress + offset;

	ReadProcessMemory(hDarkSouls, (LPCVOID)statAddr, &stat, sizeof(stat), NULL);

	return (int)stat;
}


void KenderDS1::SetStat(int offset, int value) {
	LPCVOID stat = &value;
	DWORD statAddr = (DWORD)baseAddress + offset;

	WriteProcessMemory(hDarkSouls, (LPVOID)statAddr, stat, sizeof(stat), NULL);
}